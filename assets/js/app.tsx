import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { TodoContainer } from './TodoContainer/todo-container.component';
import { TodoInput } from './TodoInput/todo-input.component';
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

ReactDOM.render(
    <TodoContainer />,
    document.getElementById('todo-container')
);

ReactDOM.render(
    <TodoInput />,
    document.getElementById('todo-input')
);
