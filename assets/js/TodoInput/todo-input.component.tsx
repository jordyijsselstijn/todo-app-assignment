import * as React from 'react';
import 'flatpickr/dist/themes/material_green.css';
import Flatpickr from 'react-flatpickr'

export class TodoInput extends React.Component<{}, { date: Date, title: string }> {
    constructor(props) {
        super(props)
        this.state = {
            date: new Date(),
            title: ''
        };
    }
    validateInput(): boolean {
        return this.state.title != '';
    }
    submitNewTodo = (e) => {
        if (!this.validateInput()) return;
        let todo = {
            title: this.state.title,
            expiryDate: this.state.date
        };
        fetch('/api/todos', {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrer: 'no-referrer',
            body: JSON.stringify(todo), // body data type must match "Content-Type" header
        }).then(res => res.json())
            .then((res) => {
                todo['id'] = res;
                todo['dateAdded'] = new Date();
                window.dispatchEvent(new CustomEvent('todo-app__new-todo-added', { detail: { todo } }))
                this.setState({ title: '' })
            })

    }
    render() {
        const { date, title } = this.state;
        return (
            <div className="todo-input">
                <input className="todo-input__title" type="text" placeholder="To do title" value={title} onChange={(event) => { this.setState({ title: event.target.value }) }} />
                <Flatpickr
                    data-enable-time
                    className="todo-input__date"
                    value={date}
                    onChange={date => { this.setState({ date: date[0] }) }} />
                <button onClick={this.submitNewTodo} className="todo-input__submit">+</button>
            </div>
        )
    }
}