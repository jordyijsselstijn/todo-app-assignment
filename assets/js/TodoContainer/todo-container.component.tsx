import * as React from 'react';
import { iTodo, Todo } from '../Todo/todo.component';

export class TodoContainer extends React.Component<any, { todos: iTodo[], todosLoaded: boolean, showDone: boolean }> {
    constructor(props: any) {
        super(props)
        this.state = {
            todos: [],
            todosLoaded: false,
            showDone: false
        }
    }
    componentDidMount() {
        fetch('/api/todos?include-done=true')
            .then(res => res.json())
            .then((todos) => {
                return todos.map((todo) => {
                    return {
                        title: todo.title,
                        dateAdded: todo.dateAdded.date,
                        expiryDate: todo.expiryDate.date,
                        done: todo.done,
                        id: todo.id
                    }
                })
            })
            .then((res) => {
                this.setState({ todos: res, todosLoaded: true });
            })
        window.addEventListener('todo-app__new-todo-added', (event: CustomEvent) => {
            let todo: iTodo = event.detail.todo;
            this.onTodoAdded(todo);
        })
        window.addEventListener('todo-app__todo-removed', (event: CustomEvent) => {
            this.onTodoRemove(event.detail);
        })
        window.addEventListener('todo-app__todo-status-toggle', (event: CustomEvent) => {
            this.onToggleTodoStatus(event.detail);
        })
    }
    onToggleTodoStatus(id) {
        let newTodolist = this.state.todos.map((todo) => {
            if (todo.id == id) {
                todo.done = !todo.done;
            }
            return todo;
        })
        this.setState({ todos: newTodolist })
        fetch('/api/todos/' + id, {
            method: 'PUT', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrer: 'no-referrer'
        });
    }
    onTodoAdded(todo: iTodo) {
        let original = this.state.todos;
        original.push(todo);
        this.setState({ todos: original });
    }
    onTodoRemove(id) {
        let newTodolist = this.state.todos.filter((todo) => {
            return todo.id != id
        })
        this.setState({ todos: newTodolist })
        fetch('/api/todos/' + id, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, cors, *same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/json',
            },
            redirect: 'follow',
            referrer: 'no-referrer'
        })
    }
    render() {
        let notDoneTodos = this.state.todos
            .filter(todo => !todo.done)
            .map((todo: iTodo) => {
                return <Todo key={todo.id} todo={todo} />
            })
        let doneTodos = this.state.todos
            .filter(todo => todo.done)
            .map((todo: iTodo) => {
                return <Todo key={todo.id} todo={todo} />
            })
        let notDone = this.state.todos.filter(todo => !todo.done);
        let done = this.state.todos.filter(todo => todo.done);

        return (
            <div>
                <div className="todo-tab__container">
                    <div className={`todo-tab ${this.state.showDone ? '' : 'active'}`}>
                        <span className="inner" onClick={() => this.setState({ showDone: false })}>To do ({notDone.length})</span>
                    </div>
                    <div className={`todo-tab ${this.state.showDone ? 'active' : ''}`}>
                        <span className="inner" onClick={() => this.setState({ showDone: true })}>Done ({done.length})</span>
                    </div>
                </div>
                <div className="todo-container">
                    {
                        (notDoneTodos.length > 0 && !this.state.showDone) &&
                        <ul className="todo-list">
                            {notDoneTodos}
                        </ul>
                    }
                    {
                        (doneTodos.length > 0 && this.state.showDone) &&
                        <ul className="todo-list">
                            {doneTodos}
                        </ul>
                    }
                    {
                        (notDoneTodos.length == 0 && this.state.todosLoaded && !this.state.showDone) &&
                        <span className="todo-list__empty">Add a to do +</span>
                    }
                </div>
            </div>

        )
    }
}
