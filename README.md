## How to run this project

- Run docker-compose up in the root of this project
- Run ``docker exec -it -u dev sf4_php bash`` in your terminal when docker-compose is finished
- Run ``cd sf4``
- Run ``yarn`` to install all npm dependencies
- Run ``composer install``
- Run ``php bin/console doctrine:migrations:migrate`` to migrate the latest database changes