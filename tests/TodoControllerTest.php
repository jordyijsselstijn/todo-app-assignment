<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\Todo;

class TodoControllerTest extends WebTestCase
{
    public function testShowTodosStatus()
    {
        $client = static::createClient();

        $client->request('GET', '/api/todos');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    public function testShowTodosWithoutDoneStatus()
    {
        $client = static::createClient();
        $client->request('GET', '/api/todos');
        $todos = json_decode($client->getResponse()->getContent());
        $done_todos = array_filter($todos, function($todo){
            return $todo->done;
        });
        $this->assertEquals(count($done_todos), 0);
    }

    public function testToggleStatus()
    {
        $id_to_test;
        $status_before_change;
        $client = static::createClient();
        $client->request('GET', '/api/todos?include-done=true');
        $todos = json_decode($client->getResponse()->getContent());
        if (count($todos) > 0)
        {
            $id_to_test = $todos[0]->id;
            $status_before_change = $todos[0]->done;
        }
        else {
            $this->markTestSkipped(
              'No todos available to test'
            );
        }
        $client->request('PUT', '/api/todos/'.$id_to_test);
        $client->request('GET', '/api/todos/'.$id_to_test);
        $changed_todo = json_decode($client->getResponse()->getContent());
        $this->assertNotEquals($status_before_change, $changed_todo->done);
    }
    public function testCreateAndDeleteTodo()
    {
        $client = static::createClient();
        $json_data = '{ "title" : "Fake Title", "expiryDate": "01-01-2019"}';
        $client->request('POST', 
                            '/api/todos',         
                            array(),
                            array(),
                            array('CONTENT_TYPE' => 'application/json'),
                            $json_data
                        );
        $todo_id = json_decode($client->getResponse()->getContent());
        $client->request('DELETE', '/api/todos/'.$todo_id);
        $client->request('GET', '/api/todos/'.$todo_id);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}