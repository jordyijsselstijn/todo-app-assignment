<?php

namespace App\Service;
use App\Entity\Todo;
use Doctrine\ORM\EntityManagerInterface;

class TodoService {
    private $entityManager;
    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    }
    public function GetTodos($includeDone = false)
    {
        $repository = $this->entityManager->getRepository(Todo::class);
        if (!$includeDone)
        {
            $todos = $repository->findDone();
        } else {
            $todos = $repository->findAll();
        }
        return $todos;
    }

    public function GetTodoById($todoId)
    {
        $todo = $this->entityManager
            ->getRepository(Todo::class)
            ->find($todoId);

        if (!$todo) {
            return null;
        } else {
            return $todo;
        }
    }
    public function CreateTodo($todo)
    {
        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $this->entityManager->persist($todo);
        // actually executes the queries (i.e. the INSERT query)
        $this->entityManager->flush();

        return $todo->getId();
    }

    public function RemoveTodo($todoId)
    {
        $todo = $this->entityManager->getRepository(Todo::class)->find($todoId);
        if (!$todo) {
            return null;
        }
        $this->entityManager->remove($todo);
        $this->entityManager->flush();
        return true;
    }
    
    public function ToggleTodoDoneStatus($todoId)
    {
        $todo = $this->entityManager->getRepository(Todo::class)->find($todoId);
        if (!$todo) {
            return null;
        }
        $todo->setDone(!$todo->getDone());
        $this->entityManager->flush();
        return $todo->getId();
    }
}