<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Psr\Log\LoggerInterface;
use App\Entity\Todo;

use App\Service\TodoService;

class TodoController extends AbstractController {

    private $todoService;
    public function __construct(TodoService $todoService)
    {
        $this->todoService = $todoService;
    }

    /**
     * @Route("/api/todos", name="get_todos", methods={"GET", "HEAD"})
     */
    public function All(Request $request)
    {

        $result = $this->todoService->GetTodos($request->query->get('include-done'));
        if ($result == null)
        {
            $result = [];
        }
        return new Response(
            json_encode($result)
        );
    }
    
    /**
     * @Route("/api/todos/{todoId}", name="get_todos_by_id", requirements={"todoId"="\d+"}, methods={"GET", "HEAD"})
     */
    public function GetById($todoId){
        $result = $this->todoService->GetTodoById($todoId);
        if ($result == null)
        {
            $response = new Response();
            $response->setStatusCode(404);
            return $response;
        }   
        return new Response(
            json_encode($result)
        );
    }

    /**
     * @Route("/api/todos", name="create_todo", requirements={"todoId"="\d+"}, methods={"POST", "HEAD"})
     */
    public function CreateTodo(Request $request){
        $_todo = json_decode($request->getContent());
        $todo = new Todo();
        $todo->setTitle($_todo->title);
        $todo->setDateAdded(new \DateTime());
        $todo->setExpiryDate(new \DateTime($_todo->expiryDate));
        $todo->setDone(false);

        return new Response(
            json_encode($this->todoService->CreateTodo($todo))    
        );
    }

    /**
     * @Route("/api/todos/{todoId}", name="remove_todo", requirements={"todoId"="\d+"}, methods={"HEAD", "DELETE"})
     */
    public function RemoveTodo($todoId)
    {
        if ($this->todoService->RemoveTodo($todoId))
        {
            $response = new Response();
            $response->setStatusCode(200);
            return $response;
        }
    }
    /**
     * @Route("/api/todos/{todoId}", name="toggle_todo_done_status", requirements={"todoId"="\d+"}, methods={"PUT", "HEAD"})
     */
    public function ToggleTodoDoneStatus($todoId){
        $result = $this->todoService->ToggleTodoDoneStatus($todoId);
        if ($result == null) {
            $response = new Response();
            $response->setStatusCode(404);
            return $response;
        }
        return new Response(
            json_encode($result)
        );
    }
}