<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class AppController extends AbstractController {

    /**
    * @Route("/", name="app", methods={"GET", "HEAD"})
    */
    public function index()
    {
        return $this->render('app/app.html.twig', []);
    }
}